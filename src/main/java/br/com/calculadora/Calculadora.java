package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public String nome;

    public Calculadora(){}

    public int soma(int primeiroNumero, int segundoNumero){
        if(primeiroNumero < 0 || segundoNumero < 0){
            throw new RuntimeException("Não pode ser negativo");
        }else{
        int resultado = primeiroNumero + segundoNumero;
        return resultado;}
    }

    public double soma(double primeiroNumeero, double segundoNumero){

        Double resultado = primeiroNumeero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public int subtracao(int primeiroNumereo, int segundoNumero){
        int resultado = (primeiroNumereo) - (segundoNumero);
        return resultado;
    }

    public double subtracao(double primeiroNumereo, double segundoNumero){
        double resultado = (primeiroNumereo) - (segundoNumero);
        return resultado;
    }


    public double divisao(int primeiroNumero, int segundoNumero) throws Exception {
        if(segundoNumero == 0){
            throw new Exception("Segundo numero não pode ser 0");
        }

        double resultado = (double)primeiroNumero / (double)segundoNumero;
        return resultado;

    }

    public double divisao(double primeiroNumero, double segundoNumero) throws Exception {
        if(segundoNumero == 0){
            throw new Exception("Segundo numero não pode ser 0");
        }

        double resultado = primeiroNumero / segundoNumero;
        return resultado;

    }

    public int multiplicacao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double multiplicacao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }


//    divisao
//    divisao por numeros negativos
//            divisao por numeros inteiros
//            divisão por numeros flutuantes
//    multiplicacao

}
