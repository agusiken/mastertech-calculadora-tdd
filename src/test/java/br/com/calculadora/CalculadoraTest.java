package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {
    //tem que ser mto claro com o que tá teestando

    private Calculadora calculadora;

    //ele executa esse BeforeEach antes de cada método
    @BeforeEach
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes() {
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaASubstracaoDeDoisNumerosInteiros(){
        int resultado = calculadora.subtracao(-1,-1);

        Assertions.assertEquals(0,resultado);
    }

    @Test
    public void testaASubstracaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.subtracao(1.5,0.5);

        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros() throws Exception {
        double resultado = calculadora.divisao(5, 2);

        Assertions.assertEquals(2.5, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosNegativos() throws Exception {
        double resultado = calculadora.divisao(-1.0, -2);

        Assertions.assertEquals(0.5, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosFlutuantes() throws Exception {
        double resultado = calculadora.divisao(1.5, 2.5);

        Assertions.assertEquals(0.60, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros() {
        int resultado = calculadora.multiplicacao(1, 3);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteirosNegativos() {
        int resultado = calculadora.multiplicacao(-1, -2);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.multiplicacao(1.5, 0.5);

        Assertions.assertEquals(0.75, resultado);
    }

    @Test
    public void testaCaminhoTristeDaSoma(){
        Assertions.assertThrows(RuntimeException.class, () -> {calculadora.soma(-1,0);});
    }
}
